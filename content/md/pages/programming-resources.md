{:title "Programming Resources"
 :layout :page
 :page-index 1
 :toc :ul
 :toc-class "li"}

### Language Sites
- [Clojure.org](https://www.clojure.org) - the official Clojure site - has a lot of great reference info for Clojure; info on the language core components, data structures, etc.
- [Clojuredocs.org](https://www.clojuredocs.org) - the de facto "read the docs" site for Clojure, with lots of usage examples (including "watch out for this corner case" examples) and see-also links
- [Kotlinlang.org](https://www.kotlinlang.org) - the Kotlin homepage - the starting point for Kotlin development

### Exercises
- [Codewars](https://www.codewars.com/) - a site with lots of exercises that provides a decent interface, a wide range of languages, and a feature I really like, which is that after you complete a challenge, you can see other people's solutions, which can provide insight into different ways to solve the same problem
- [Hackerrank](https://www.hackerrank.com/) - lots of challenges, a decent range of languages, but the testing of the challenges is a little more opaque than Codewars
- [Exercism](https://exercism.io/) - fewer challenges, but some of the challenges are aimed directly at the language you're working with, which can help to expose some key language features/ways of "thinking in a language". Also, Exercism has a local client that gives you a 'project' in which to work, so it can often be easier to work in an editor/IDE that you like.

### Discord
- [The Programmer's Hangout](https://discord.gg/programming) - probably the biggest
  programming Discord server - has specific channels for a wide range of languages,
  and a lot of people who are very good with mainstream languages (C*, Java,
  Python, Kotlin, JS), but not as many people who are well-versed in languages with
  smaller communities. A bit more heavily moderated than the other servers (mostly 
  aimed at keeping the community friendly).
- [Functional Programming](https://discord.gg/K6XHBSh) - lots of FP and dependently-typed
  language channels and lots of knowledgeable people. One downside is that a vocal
  minority of members can be abrasive, derisive, and/or condescending. 
- [Clojure Discord](https://discordapp.com/invite/v9QMy9D) - a friendly Discord for
  Clojurians. Lots of nice people, but basically no moderation or administration.
- [Discljord](https://discord.gg/discljord) - another friendly Discord for
  Clojurians. Its titular purpose is for a Clojure library for Discord bots, but it
  also has active channels for general Clojure discussion and getting help and so on.
