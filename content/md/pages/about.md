{ :title "About"
  :layout :page
  :page-index 0
  :navbar? true }

### Bob
At work, I'm a software engineer. A lot of the software I work on these days is software that's about software (automated tests, devops, auditing tools, etc).
I also like to advocate for simplicity (composing, not complecting) and help colleagues make their software work better (for almost any aspect of 'better').

Outside of work, I still like software. I especially like helping others improve their knowledge of programming, because not only does it help the other person,
but it helps me to understand my views even better. I grew up messing around in Visual
Basic, then moved on to Java (and a handful of other OO languages), and then to Kotlin,
and now I try to use Clojure as much as I can. 

I also like fast cars, and have spent some time at the race track and doing autocross.

### Contact
email: [bobsgeekblog@gmail.com](mailto:bobsgeekblog@gmail.com)

### Code
GitLab: [https://gitlab.com/bobisageek](https://gitlab.com/bobisageek)

GitHub: [https://github.com/bobisageek](https://github.com/bobisageek)
