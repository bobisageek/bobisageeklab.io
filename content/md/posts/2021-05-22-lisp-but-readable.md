{:title "Rant: \"Lisp, But Readable\""
 :layout :post
 :date "2021-05-22"
 :tags  ["rants" "lisp"]
 :toc false}


## Background

I read a blog article yesterday about Elixir macros, and there was a sentence in there
that got me thinking. The post references lisp macros as a comparison, and then says in passing that "Elixir is _basically_ LISP but readable".

If feels to me like maybe this sentence was thrown in as just a conclusion to the paragraph, but it also
feels to me like a sentence full of "weasel words". "Readable" feels a lot like a euphemism for "familiar to
me" - as a native speaker of American English, I might say that English is "readable" to me, and some other
language is "unreadable", but that's because I haven't taken the time to learn to read it. Meanwhile, the
"basically" is an escape hatch for making an assertion and then saying "well, I wasn't _actually_ saying that"
if challenged on it.

Like I said, I doubt there was malice in the statement, and I don't know about the author's experience with lisps, but I'd like to look
at some examples for comparing and contrasting in terms of 'readability'.

## A **Brief** Overview of 'Macros'

There are many examples of things called 'macros' in programming languages. We can look back to C macros, which
are something on the order of string replacement as a compiler directive. C++ has template metaprogramming,
which is more robust than a find/replace setup, but is, in some ways, almost a separate language from C++
itself. 

A handful of languages (OCaml, Elixir, and Julia to name a few) have the idea of a macro that's 
roughly an AST -> AST transformation, but the 'normal' code in these languages is not an AST, which means that
in order to understand macro output, you need to be able to read not only code, but also the ASTs. On the other
hand, lisp code is made up of data structures, and macros are data structure -> data structure transformations,
so if you can read code, you can read quoting/macro output, because they're made of the same stuff.

## Some Comparisons

To keep things short at first, let's look at some math - I'm going to use Elixir and Clojure, because I have them both at hand:

### Evaluating

#### Elixir
```elixir
iex> 5 + 3
=> 8
```

#### Clojure
```clojure
user> (+ 5 3)
=> 8
```

I'm sure there are tomes extolling the virtues of operators being infix or prefix or postfix, but without getting 
very editorial, I think these are both pretty straightforward to grok, and when you consider that the Elixir version
is syntax sugar for `Kernel.+(5, 3)`, readability arguments here are going to boil down to parenthesis placement.

### Quoting (and Evaluating the Outcomes)

#### Elixir Example 1
```elixir
iex> quote do 5 + 3 end
{:+, [context: Elixir, import: Kernel], [5, 3]}

# and feed the output back in

iex> {:+, [context: Elixir, import: Kernel], [5, 3]}
{:+, [context: Elixir, import: Kernel], [5, 3]}
```

#### Clojure Example 1
```clojure
user> `(+ 5 3)
=> (clojure.core/+ 5 3)

; and feed the output back in

user> (clojure.core/+ 5 3)
=> 8
```

Note that the Elixir AST looks a bit different, so when we talk about readability, we're talking about the readability of two quite
different things. Clojure quoting (in this case, syntax quoting to resolve the namespace to maximize parity between the examples) still
returns a data structure that works as code - so much so that just executing the output of quoting returns the answer. 

Small math problems generate small ASTs, so let's look at something just a little bigger - defining a function that does some math:

#### Elixir Example 2
```elixir
iex> quote do def sum(a,b), do: a + b end
{:def, [context: Elixir, import: Kernel],
 [
  {:sum, [context: Elixir], [{:a, [], Elixir}, {:b, [], Elixir}]},
  [
   do: {:+, [context: Elixir, import: Kernel],
    [{:a, [], Elixir}, {:b, [], Elixir}]}
  ]
 ]}
```

#### Clojure Example 2
```clojure
user> (macroexpand-1 `(defn ~'sum [~'a ~'b] (+ ~'a ~'b)))
=> (def sum (clojure.core/fn ([a b] (clojure.core/+ a b))))
```

Same idea here - while there's a bit of extra stuff in the Clojure version to avoid some namespace resolution, the output
a) requires roughly the same reading as the input, and b) is executable.

### Conclusion

To me, this concept is a key differentiator between lisps and (most) other languages with macros - they all generally have some form of 
quoting, but because lisp code is repesented as data, the quoting mechanism can return new data, and it's code. Other languages
have quoting mechanisms that return data, but that data is often wildly different from the code, which means that we need to "read a second 
language" sometimes, and that we need something else to actually "run" that data as code. I'm not going to say what's "more readable", because that's subjective, but I will say that I feel like I learned to read Elixir, and then later learned to read Elixir ASTs. With Clojure, I learned to read Clojure, and I haven't yet hit anything new to learn how to read.

