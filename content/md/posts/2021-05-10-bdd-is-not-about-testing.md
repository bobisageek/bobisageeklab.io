{:title "Conference Talk: BDD is not about Testing"
 :layout :post
 :date "2021-05-10"
 :tags  ["testing" "conference talks"]
 :toc false}

#### Talk title: "BDD is not about Testing" 
#### Presenter: Daniel Terhorst-North

<iframe class="youtube-talk" width="560" height="315" src="https://www.youtube.com/embed/6nSwRSbc27g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Daniel is one of my favorite all-around conference speakers. His talks cover a wide
variety of topics, from Linux command-line amazingness to team dynamics to how to
exploit innovation. Daniel is also widely credited as originating BDD, and in this talk
he goes deep into his definition of testing and where BDD does and doesn't fit in to
that picture.
