{:title "Coding Exercise: Rotating a Vector"
 :layout :post
 :date "2021-10-24"
 :tags  ["clojure" "coding exercises"]
 :toc false}

### Context
A question came up the other day about rotating the elements of a vector, so `[1 2 3]` becomes `[2 3 1]`, for example. 

### Solutions
On the surface, there's a pretty straightforward implementation for this:
```clojure
(defn rotate-vec [v]
  (conj (subvec v 1) (first v)))

(rotate-vec [1 2 3])
=> [2 3 1]
```

This has some edge cases, however. Namely, an empty vector will throw an IndexOutOfBoundsException, and a one-element vector is kind of wasteful to rotate, because you'll always end up with the original vector. I'm going to say that rotating an empty vector returns an empty vector, and so we can handle both edge cases with an `if`:
```clojure
(defn rotate-vec [v]
  (if (> 2 (count v)) 
    v
    (conj (subvec v 1) (first v))))

(rotate-vec [])
=> []
(rotate-vec [1 2 3])
=> [2 3 1]
```

### Generalizing
Probably the most obvious generalization is to rotate some number of times. There are a number of ways to accomplish this, with small trade-offs between brevity and efficiency. We could write a longer, somewhat more imperative thing with modulus:
```clojure
(defn rotate-vec 
  ([v] (rotate-vec v 1))
  ([v n]
   (if (> 2 (count v))
     v
     (let [split-index (mod n (count v))]
       (into (subvec v split-index) (subvec v 0 split-index))))))
```
My preference from an elegance perspective is to make an infinite sequence of rotations and then just take the rotation that corresponds to how many rotations are desired:
```clojure
(defn rotations [v]
  (let [n (max 1 (count v)) ; account for an empty vector
        r (take n (iterate #(conj (subvec % 1) (first %)) v))]
    (cycle r)))

(take 5 (rotations []))
=> ([] [] [] [] [])
(nth (rotations [1 2 3]) 5)
=> [3 1 2]
```
Things I like about this:
- it handles the edge cases just by making sure to take at least one. If the vector has one or fewer elements, we'll only take the first element of iterating the function, which is just the original vector
- since the function returns a sequence, in the case where you might need a bunch of rotations of the same vector, you don't have to recompute them

### Wrapping Up
There are many ways one write this function, and which one is "best" is largely dependent on context, but Clojure gives you ways to do it in an efficient, specific way, or in a general "how would you like it?" way.
