{:title "Rant: Dogmatic Scrum"
 :layout :post
 :date "2021-06-23"
 :tags  ["rants" "agile" "scrum"]
 :toc false}


This is not a rant about Scrum in and of itself - the rant is more about Scrum when practiced as a dogma, and the core agile manifesto of "Responding to change over following a plan" is ignored.

When a story is almost done at the end of a sprint, but needs to be wrapped up the day after the sprint ends, what do you do with that story? It is my opinion that you leave that story in the sprint where it got mostly done, and complete it as soon as it's wrapped up. This provides the best depiction of what happened without being wasteful. 

Another option is to say "well, the story didn't get done in that sprint, so it has to go to the next sprint". This provides a poor representation of when the work was done, because now it looks like the whole story happened in the sprint where almost none of the work happened.

A wasteful option is to split the story, creating a new story to tie up an hour worth of loose ends. This bloats the story count, and means someone spends their time creating the new story, just to complete it the next day.

So why waste the time creating this extra story? Because the Scrum police say that all the stories in a sprint have to be completed by the end of the day on the last day of the sprint, or you end up on some report that goes to management. This incentivizes things you probably don't want. Let's say there are two days left in the sprint, and I finish the story I'm working on. I look for the next story in the list, and if I don't know if I can finish the story this sprint, I might start working on it, but I'm not going to acknowledge that I'm working on it. I might even just say "well, I'm not going to start working on this story, because it's definitely going to carry over to the next sprint". 

In my opinion, this dogmatic view that one piece of work that crosses sprints needs to be treated as multiple pieces of work creates inefficiencies, by encouraging people to say "if I can't finish this by Friday, I'm not going to start on it until Monday".

One reason that a lot of shops adopt these methodologies that supposedly encourage agility is because projects can be these big chunks of work where we push forward for three months before getting any feedback, and any feedback we get becomes a new project. To me, this is a core finding underlying the tenet of "Responding to change over following a plan". This strategy of "split the story up based on an inflexible calendar" feels like it's very much the same problem, but executed more frequently.
