{:title "Conference Talk: CLJS in the Age of TypeScript"
 :layout :post
 :date "2021-08-08"
 :tags  ["clojure" "conference talks"]
 :toc false}

#### Talk title: "ClojureScript in the Age of TypeScript" 
#### Presenter: David Nolen

<iframe class="youtube-talk" width="560" height="315" src="https://www.youtube.com/embed/3HxVMGaiZbc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


David Nolen, a core ClojureScript developer, talks about his (and others) experience using ClojureScript at Vouch.io, and why, 
at a time where the industry seems to be going wild over stronger types (more typing in Python, Scala, TypeScript), Clojure/Script's 
ongoing focus on immutability and late binding/dynamism is providing developers with capabilities that are still missing from most of 
the most popular languages.
