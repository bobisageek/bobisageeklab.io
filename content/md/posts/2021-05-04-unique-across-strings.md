{:title "Coding Exercise: Unique Characters Across Strings"
 :layout :post
 :date "2021-05-04"
 :tags  ["clojure" "coding exercises"]
 :toc false}

### Background
- Difficulty: Easy
- Language: Clojure

### Problem statement
We'll be given two strings containing characters from 'a' through 'z', and expected
to return a string that contains all of the characters in either of the strings, sorted alphabetically.

#### Examples
- strings "abc" and "def" => "abcdef"
- strings "youaie" and "iaeyou" => "aeiouy"
- strings "toy" and "fort" => "forty"

### Approach
#### The Individual Parts
As Stuart Halloway has said, one thing we often do in Clojure is to make the
problem more general, and then solve the more general problem. In my experience,
this is often true when working with sequences (or things that we can treat as
seqs). In this case, I'm going to generalize the problem to "take any number of
sequences of things that can be sorted (strings can be treated as sequences of
characters), get all the unique things from those sequences, and sort those unique
things". From there, putting those things into a string is a straightforward exercise. 

First things first, I want to combine my sequential things into one sequential thing. This is easy enough with `concat`. 
```clojure
(concat "cba" "fed")
=> (\c \b \a \f \e \d)
```
`concat` is variable-arity - this will come in handy when we get to our "any number of sequences" part.

With one sequence, we need to remove duplicates and sort. Sorting typically has greater than O(n) time complexity, while
removing duplicates can be O(n), so we'll remove duplicates first, and then sort what might be fewer items. Clojure has 
two somewhat similar functions for removing duplicates from a list, but there's a subtle difference:
- `distinct` returns a sequence of items unique across the sequence
- `dedupe` only removes _consecutive_ duplicates, so `(dedupe [1 1 2 2 1 1 3])` will return `(1 2 1 3)`, because the remaining 
  ones are not consecutive
  
If we were sorting first, then the behavior between the functions would match up, but since we're not, we'll just use `distinct`.

#### Putting the Parts Together
There are a couple of ways to write the function, all of which return the same results:
```clojure
(defn sort-uniques [& seqs]
  (sort (distinct (apply concat seqs))))

(defn sort-uniques-thread [& seqs]
  (-> (apply concat seqs)
    distinct
    sort))

(def sort-uniques-comp (comp sort distinct concat))
```

Each of these functions will return a sorted sequence of unique items, and they work on strings, vectors, and other 'seqable' things.
Our initial problem statement requires that we return a string, so to go from our sequence to a string, we'll just `apply str`. The whole thing comes together to:
```clojure
(def sort-unique-chars (comp (partial apply str) sort-uniques))
```

... and we can run a couple tests at the REPL to ensure our function works as expected:
```clojure
(sort-unique-chars "fad" "ebc")
=> "abcdef"
(sort-unique-chars "fort" "toy")
=> "forty"
(sort-unique-chars "pom" "lon" "nop" "nom")
=> "lmnop"
```

#### Conclusions
This is a very straightforward challenge, especially in a language like Clojure, which handles sequences well. And this is
just one approach that Clojure makes available - we could also have solved this using sets, and `clojure.set/union` would have 
mashed our sets together into one. 

Even on such an easy exercise, we can see a couple of takeaways:
- It can often be helpful to think about a solution to a more general problem, and then use a small transformation to specialize
  for the specific situation.
- Clojure has a wide selection of core functions that do one thing, and they can be composed to accomplish a wide variety of 
  more complex tasks.

  
