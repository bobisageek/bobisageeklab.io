{:title "Conference Talk: Simple Made Easy"
 :layout :post
 :date "2021-05-09"
 :tags  ["clojure" "conference talks"]
 :toc false}

#### Talk title: "Simple Made Easy" 
#### Presenter: Rich Hickey

<iframe class="youtube-talk" width="560" height="315" src="https://www.youtube.com/embed/kGlVcSMgtV4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


In this talk, Rich Hickey starts by defining 'simple' in opposition to 'easy'. He then
discusses the value proposition of simplicity, and contrasts language features that are
complex with things that are simpler. This talk is from 2011, so it's quite old, but I
think that, to this day, it's still possibly one of the best introductions to the
rationale underlying a lot of the design decisions in Clojure, and it can provide some talking points for discussing functional programming with programmers who don't have a lot of exposure to it.
