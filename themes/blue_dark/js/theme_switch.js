const theme_button = document.querySelector("#theme-switch");

const main_theme = document.querySelector("#main-styles")
const code_theme = document.querySelector("#code-styles")

const themes = {
    "dark": {
        "main": "/css/dark.css",
        "code": "/css/dark-code.css"
    },
    "light": {
        "main": "/css/light.css",
        "code": "/css/light-code.css"
    }
}

function change_theme_to(mode) {
    main_theme.href = themes[mode].main;
    code_theme.href = themes[mode].code;
    localStorage.setItem("theme", mode);
}

function toggle_theme() {
    if (main_theme.getAttribute("href") === "/css/dark.css") {
        change_theme_to("light");
    } else {
        change_theme_to("dark");
    }
}

function set_theme_on_page_load() {
    let stored_theme = localStorage.getItem("theme") || "dark";
    change_theme_to(stored_theme);
}

set_theme_on_page_load();
theme_button.addEventListener("click", toggle_theme);
